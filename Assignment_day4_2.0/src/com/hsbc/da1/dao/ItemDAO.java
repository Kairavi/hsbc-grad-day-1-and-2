package com.hsbc.da1.dao;

import com.hsbc.da1.model.Item;

public interface ItemDAO {
	Item saveItem(Item item);
	
	void deleteItem(long itemId);
	
	Item updateItem(long itemId,Item item);
	
	Item[] fetchAllItems();
	
	Item fetchItemById(long itemId);
	

}

