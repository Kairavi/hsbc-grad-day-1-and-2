package com.hsbc.da1.controller;

import com.hsbc.da1.model.Item;
import com.hsbc.da1.service.ItemService;
import com.hsbc.da1.service.ItemServiceImpli;

public class ItemController {
	ItemService service = new ItemServiceImpli();
	
	public Item registerItem(String itemName,double itemPrice) {
		return this.service.createItem(itemName, itemPrice);
	}
	
	public void deleteItem(long itemId) {
		this.service.deleteItem(itemId);
	}
	
	public Item[] fetchAllItems() {
		return this.service.fetchAllItems();
	}
	
	public Item fetchItemById(long itemId) {
		return this.service.fetchItemById(itemId);
	}
	
	public double checkPrice(long itemId) {
		// TODO Auto-generated method stub
		
		return this.service.fetchItemById(itemId).getItemPrice();
	}

	public Item updateNameOfItem(long itemId,String itemName) {
		// TODO Auto-generated method stub
		return this.service.updateNameOfItem(itemId, itemName);
	}

}
