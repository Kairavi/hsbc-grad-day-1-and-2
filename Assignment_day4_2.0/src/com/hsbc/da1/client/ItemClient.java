package com.hsbc.da1.client;

import com.hsbc.da1.controller.ItemController;
import com.hsbc.da1.model.Item;

public class ItemClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ItemController controller = new ItemController();
		Item pencil = controller.registerItem("pencil", 25.00);

		Item penstand = controller.registerItem("penstand", 30.00);
		
		Item pen = controller.registerItem("pen", 70.00);

		Item refill = controller.registerItem("refil", 0.78);

		Item[] allItems = controller.fetchAllItems();

		for (Item item : allItems) {
			if (item != null) {
				System.out.println("Name of item - " + item.getItemName());
				System.out.println("price of item - " + item.getItemPrice());
			} else {
				break;
			}
		}

	}

}
