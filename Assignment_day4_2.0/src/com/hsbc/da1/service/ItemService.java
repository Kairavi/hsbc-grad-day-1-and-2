package com.hsbc.da1.service;

import com.hsbc.da1.model.Item;

public interface ItemService {
	
	Item createItem(String itemName,double itemPrice);
	
	void deleteItem(long itemId);
	
	Item[] fetchAllItems();
	
	Item fetchItemById(long itemId);
	
	double checkPrice(long itemId);
	
	Item updateNameOfItem(long itemId,String itemName);

}
