abstract class BankAccount {

    private  final long accountNumber;
    private static long counter = 1000;

    private double accountBalance;

    private String BusinessName;

    private String customerName;

    //private Address address;

    private String emailAddress;

    private String nominee;

    private String nationality;

    
    public abstract void withdrawlLimit();
    public abstract void minimumBalance ();
    public abstract void loanEligibility ();
    public abstract void showAllDetails ();
    
    public abstract void grantLoan(double amount);
    public abstract void showBalance();

    public  boolean validateBalance(int limit,double amount)
    {
        if((this.accountBalance-limit >= amount))
        {
            return true;
        }
        else
        {
            return false;
        }
    
    }

    public  void deposit(double amount)
    {
        this.accountBalance = this.accountBalance + amount;
    }

    public final String getCustomerDetails()
    {
        return "Customer name" + this.customerName + "balance :" + this.accountBalance +"A/C number :" +this.accountNumber;
    }
      
    public BankAccount(String customerName,double accountBalance)
    {
        this.customerName = customerName;
        this.accountBalance = accountBalance;
        this.accountNumber = ++counter;

    }
    public void withdraw(double amount)
    {
        if(validateBalance(limit,amount))
        {
            this.accountBalance = this.accountBalance - amount;
        }
        return ;

    }



}
////-------------------------------------SAVINGS ACCOUNT-------------------------------------------------
class SavingsAccount extends BankAccount 
{

    public SavingsAccount(String customerName,double accountBalance)
    {
        super(customerName,accountBalance);
    }


    public void showAllDetails ()
    {
        System.out.println("The benifits of Savings Account are :");
        withdrawlLimit();
        minimumBalance();
        loanEligibility();
    }
    


    public void grantLoan( double amount)
    {
        if(amount <= 500000)
        {
            System.out.println("You are eligible for the loan ");
            super.deposit(amount);
        }
        else
        {
            System.out.println("You are not eligible for the loan ");

        }

    }

    public void withdraw(double amount)
    {     
        int limit = 10_000;

        if(super.validateBalance(10_000,amount))
        {
            super.withdraw(limit,amount);
        }
       

    }


    public void withdrawlLimit() {
        System.out.println("The withdrawl limit is 10_000");
    }

    public void minimumBalance() {
        System.out.println("The minimum balance required is 10_000 ");
    }

    public void loanEligibility() {
        System.out.println("You are allowed to take loan upto 5 Lacs");
    }

    

}
 
////-------------------------------------CURRENT ACCOUNT-------------------------------------------------

class CurrentAccount extends BankAccount {

    private String gstNumber;

    public CurrentAccount(String customerName,double accountBalance,String gstNumber)
    {
        super(customerName,accountBalance);
        this.gstNumber = gstNumber;
    }

    public void showAllDetails ()
    {
        System.out.println("The benifits of Current Account are :");
        withdrawlLimit();
        minimumBalance();
        loanEligibility();
    }

    public void withdraw(double amount)
    {   
        int limit = 25_000;
        super.withdraw(limit,amount);

    }

    public void grantLoan(double amount)
    {
        if(amount <= 2500000)
        {
            System.out.println("You are eligible for the loan ");
            super.deposit(amount);
        }
        else
        {
            System.out.println("You are not eligible for the loan ");

        }

    }


    public void withdrawlLimit() {
        System.out.println("You have no withdrawl limit");
    }

    public void minimumBalance() {
        System.out.println("The minimum balance required is 25_000 ");
    }

    public void loanEligibility() {
        System.out.println("You are allowed to take loan upto 25 Lacs");
    }

}

////-------------------------------------SALARIED ACCOUNT-------------------------------------------------

class SalariedAccount extends BankAccount {

    public SalariedAccount(String customerName,double accountBalance);
    {
        super(customerName,accountBalance);
    }

    public void showAllDetails ()
    {
        System.out.println("The benifits of Salaried Account are :");
        withdrawlLimit();
        minimumBalance();
        loanEligibility();
    }

    public void withdraw(double amount)
    {   
        int limit = 15_000;
        super.withdraw(limit,amount);

    }

    public void grantLoan( double amount)
    {
        if(amount <= 1500000)
        {
            System.out.println("You are eligible for the loan ");
            super.deposit(amount);
        }
        else
        {
            System.out.println("You are not eligible for the loan ");

        }

    }

    

    @Override
    public void withdrawlLimit() {
        System.out.println("The withdrawl limit is 15_000");
    }

    public void minimumBalance() {
        System.out.println("No need to maintain any minimum balance ");
    }

    public void loanEligibility() {
        System.out.println("You are allowed to take loan upto 10 Lacs");
    }

}

////-------------------------------------MAIN FUNCTION-------------------------------------------------

public class BankAccountDetails {

    public static void main(String[] args) {
        // datatype var = new DataType();
        // Doctor doc = new Doctor();
        // doc.treatPatient();

        //Orthopedecian ortho = new Orthopedecian();
        /*
         * ortho.treatPatient(); ortho.conductCTScan(); ortho.conductXRay();
         */

        BankAccount account = null;
        String typeOfAccount = args[0];
        
        account.deposit(2000);

        switch (typeOfAccount)
        {
            case "1":
                account = new CurrentAccount("Kairavi",267899,"265FYTBL");
                break;
            case "2":
                account = new SavingsAccount("Krish",25278);
                break;
            case "3":
                account = new SalariedAccount("Vinay",27588);
                break;
            default:
                System.out.println("invalid response");

        }

        account.getCustomerDetails();
        account.withdraw(400);


        /*if (inputAccountType == 1) {
            account = new CurrentAccount();
        } 
        else if (inputAccountType == 2) {
            account = new SavingsAccount();
        } else {
            account = new SalariedAccount();
        }
        */

        //execute(account);
    }

    //public static void execute(BankAccount acc) {
        /*
         * if (doc instanceof Orthopedecian) { Orthopedecian orhtopedician =
         * (Orthopedecian) doc; orhtopedician.conductCTScan();
         * orhtopedician.conductXRay(); }
         */
        //acc.showAllDetails();
        

    }
