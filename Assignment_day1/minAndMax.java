public class minAndMax {

    public static void main(String args[]) {
        String firstInput = args[0];
        String secondInput = args[1];
        String thirdInput = args[2];
        String fourthInput = args[3];
        String fifthInput = args[4];

        int arrayOfNumbers [] = new int[6];
        arrayOfNumbers[0] = Integer.parseInt(firstInput);
        arrayOfNumbers[1] = Integer.parseInt(secondInput);
        arrayOfNumbers[2] = Integer.parseInt(thirdInput);
        arrayOfNumbers[3] = Integer.parseInt(fourthInput);
        arrayOfNumbers[4] = Integer.parseInt(fifthInput);

        int maxNumber = 0;
        int minNumber = 12367;

        for(int index = 0; index < 5;index++){
            if (arrayOfNumbers[index]>maxNumber){
                maxNumber = arrayOfNumbers[index];
            }
        }

        for(int index =0; index < 5;index++){
            if(arrayOfNumbers[index]<minNumber){
                minNumber = arrayOfNumbers[index];
            }
        }

        System.out.println("The maximum number out of the list is"+maxNumber);
        System.out.println("The minimum number out of the list is"+minNumber);
    }
}


    