package com.hsbc.empmgt.service;

import java.util.Collection;

import com.hsbc.empmgmt.exceptions.InsufficientLeavesException;
import com.hsbc.empmgmt.exceptions.EmployeeNotFoundException;
import com.hsbc.empmgt.model.EmployeeAccount;

public interface EmployeeAccountService {

	EmployeeAccount saveEmployeeAccount(EmployeeAccount employeeAccout);
	
	EmployeeAccount updateEmployeeAccount(long employeeId, EmployeeAccount employeeAccount);
	
    void deleteEmployeeAccount(long employeeId);
    

    public Collection<EmployeeAccount> fetchEmpoyeeAccounts();


    EmployeeAccount fetchEmployeeAccountByEmployeeId(long employeeId)throws InsufficientLeavesException, EmployeeNotFoundException;
    
    //throws CustomerNotFoundException;
    EmployeeAccount fetchEmployeeAccountByEmployeeName(String EmployeeName) throws InsufficientLeavesException, EmployeeNotFoundException;
    

	public int takeLeaves(long accountId, int requestedDaysForLeave)throws InsufficientLeavesException, EmployeeNotFoundException; 
	// throws InsufficientBalanceException, CustomerNotFoundException;

	public double checkNoOFLeaves(long accountId, int requestedDaysForLeave)throws InsufficientLeavesException, EmployeeNotFoundException;

	EmployeeAccount createEmployeeAccount(String employeeName, double employeeSalary);

}
	
	
	
	

