package com.hsbc.empmgt.service;

import java.util.Collection;

import com.hsbc.empmgmt.exceptions.EmployeeNotFoundException;
import com.hsbc.empmgmt.exceptions.InsufficientLeavesException;
import com.hsbc.empmgt.dao.EmployeeAccountDAO;
import com.hsbc.empmgt.model.EmployeeAccount;

public class EmployeeAccountServiceImpl implements EmployeeAccountService {
	
private EmployeeAccountDAO dao;
	
	public EmployeeAccountServiceImpl(EmployeeAccountDAO dao) {
		this.dao = dao;
	}

	public EmployeeAccount createEmployeeAccount (String employeeName , double employeeSalary)
		{
		 EmployeeAccount employeeAccount = new EmployeeAccount(employeeName, employeeSalary);

		 EmployeeAccount employeeAccoutCreated = this.dao.saveEmployeeAccount(employeeAccount);
		return employeeAccoutCreated;
	}
	

	@Override
	public EmployeeAccount updateEmployeeAccount(long employeeId, EmployeeAccount employeeAccount) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteEmployeeAccount(long employeeId) {
		// TODO Auto-generated method stub
		this.dao.deleteEmployeeAccount(employeeId);
		
	}

	@Override
	public Collection<EmployeeAccount> fetchEmpoyeeAccounts() {
		// TODO Auto-generated method stub
		return this.dao.fetchEmpoyeeAccounts();
	}

	@Override
	public EmployeeAccount fetchEmployeeAccountByEmployeeId(long employeeId) throws EmployeeNotFoundException, InsufficientLeavesException {
		// TODO Auto-generated method stub
		EmployeeAccount employeeAccount = null;
		try {
			employeeAccount = this.dao.fetchEmployeeAccountByEmployeeId(employeeId);
		} catch (EmployeeNotFoundException e) {
			
		}
		return employeeAccount;
	}
	

	@Override
	public EmployeeAccount fetchEmployeeAccountByEmployeeName(String EmployeeName) throws EmployeeNotFoundException, InsufficientLeavesException {
		EmployeeAccount employeeAccount = null;
		try {
			 employeeAccount = this.dao.fetchEmployeeAccountByEmployeeName(EmployeeName);
		} catch (EmployeeNotFoundException e) {
			
		}
		return employeeAccount;
	}

	@Override
	public int takeLeaves(long employeeId, int requestedDaysForLeave) throws InsufficientLeavesException, EmployeeNotFoundException {
		EmployeeAccount employeeAccount = this.dao.fetchEmployeeAccountByEmployeeId(employeeId);
		if (employeeAccount != null) {
			double currentLeaves = employeeAccount.getNoOfLeaves();
			if (currentLeaves <= 40 || currentLeaves <= 10) {
				currentLeaves = currentLeaves - requestedDaysForLeave;
				employeeAccount.setNoOfLeaves(currentLeaves);
				this.dao.updateEmployeeAccount(employeeId, employeeAccount);
				return requestedDaysForLeave;
			} else {
				throw new InsufficientLeavesException("Do not have sufficient Leaves");
			}
		}
		return 0;
	}

	@Override
	public double checkNoOFLeaves(long employeeId, int requestedDaysForLeave)throws EmployeeNotFoundException, InsufficientLeavesException
	{
	 EmployeeAccount employeeAccount = this.dao.fetchEmployeeAccountByEmployeeId(employeeId);
		if (employeeAccount != null) {
			return employeeAccount.getNoOfLeaves();
		}
		return 0;
	}

	@Override
	public EmployeeAccount saveEmployeeAccount(EmployeeAccount employeeAccout) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
