package com.hsbc.empmgt.model;

public class EmployeeAccount
{

private String employeeName;
	
	private long employeeId;
	
	private int NoOfLeaves = 40;
	
	private double employeeSalary;
	
	private static long counter = 1000;
	
	
	
	public EmployeeAccount(String employeeName, double employeeSalary)
	{
		this.employeeName = employeeName;
		this.employeeSalary =employeeSalary;
		this.employeeId = ++counter;
	}
	
	public String getEmployeeName() {
		return this.employeeName;
	}

	public void setEmployeeName(String employeeName) {
		this.employeeName = employeeName;
	}

	public long getEmployeeId() {
		return this.employeeId;
	}

	public int getNoOfLeaves() {
		return this.NoOfLeaves;
	}

	

	public void setNoOfLeaves(double currentLeaves) {
		double d = currentLeaves = currentLeaves;
	}

	public double getEmployeeSalary() {
		return this.employeeSalary;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (employeeId ^ (employeeId >>> 32));
		result = prime * result + ((employeeName == null) ? 0 : employeeName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EmployeeAccount other = (EmployeeAccount) obj;
		if (employeeId != other.employeeId)
			return false;
		if (employeeName == null) {
			if (other.employeeName != null)
				return false;
		} else if (!employeeName.equals(other.employeeName))
			return false;
		return true;
	}

	
}
