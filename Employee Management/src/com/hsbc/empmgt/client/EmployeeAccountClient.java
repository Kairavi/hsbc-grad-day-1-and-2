package com.hsbc.empmgt.client;

import java.util.Scanner;

import com.hsbc.empmgmt.exceptions.EmployeeNotFoundException;
import com.hsbc.empmgmt.exceptions.InsufficientLeavesException;
import com.hsbc.empmgt.model.*;
import com.hsbc.empmgt.controller.*;
import com.hsbc.empmgt.model.EmployeeAccount;
import com.hsbc.empmgt.service.EmployeeAccountService;

public class EmployeeAccountClient {

	
public static void main(String[] args) throws InsufficientLeavesException, EmployeeNotFoundException {
	
	EmployeeAccountService employeeAccountService = new EmployeeAccountService();
	
		
	   EmployeeAccountController controller = new EmployeeAccountController(EmployeeAccountService employeeAccountService);
		
		EmployeeAccount KairaviEmployeeAccount = controller.openEmployeeAccount("Kairavi", 25_00_000);
		EmployeeAccount ChloeSavingsAccount = controller.openEmployeeAccount("Chloe", 45_000);
		try {
			controller.takeLeaves(KairaviEmployeeAccount.getEmployeeId(), 22);
		} catch(InsufficientLeavesException | EmployeeNotFoundException exception) {
			System.out.println("Insuffiencient leaves and leaves should be less than 10");
		}
	
	
	
	
}

}
