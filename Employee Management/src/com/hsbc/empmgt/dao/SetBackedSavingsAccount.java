package com.hsbc.empmgt.dao;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.hsbc.empmgt.model.EmployeeAccount;

public class SetBackedSavingsAccount implements EmployeeAccountDAO {

	Set<EmployeeAccount> EmployeeAccountset = new TreeSet<>();

	@Override
	public EmployeeAccount saveEmployeeAccount(EmployeeAccount employeeAccout) {
		this.EmployeeAccountset.add(employeeAccout);
		return employeeAccout;
	}

	@Override
	public EmployeeAccount updateEmployeeAccount(long employeeId, EmployeeAccount employeeAccount) {
		for(EmployeeAccount ea: EmployeeAccountset) {
			if (ea.getEmployeeId() == employeeId) {
				ea = employeeAccount;
			}
		}
		return employeeAccount;
	}

	@Override
	public void deleteEmployeeAccount(long employeeId) {
		for(EmployeeAccount ea: EmployeeAccountset) 
		{
			if (ea.getEmployeeId() == employeeId) 
			{
				this.EmployeeAccountset.remove(ea);
		
			}
		}
	}

	@Override
	public Set<EmployeeAccount> fetchEmpoyeeAccounts() {
		return this.EmployeeAccountset;
	}

	@Override
	public EmployeeAccount fetchEmployeeAccountByEmployeeId(long employeeId) {
		for(EmployeeAccount ea: EmployeeAccountset) {
			if (ea.getEmployeeId() == employeeId) {
				return ea;
			}
		}
		return null;
	}

	@Override
	public EmployeeAccount fetchEmployeeAccountByEmployeeName(String EmployeeName) {
		// TODO Auto-generated method stub
		for(EmployeeAccount ea: EmployeeAccountset) {
			if (ea.getEmployeeName() == EmployeeName) {
				return ea;
			}
		}
		return null;
	}
	}

	
	
	
	
	/*@Override
	public EmployeeAccount(SavingsAccount savingsAccount) {
		this.savingsAccountList.add(savingsAccount);
		return savingsAccount;
	}

	@Override
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for(SavingsAccount sa: savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				sa = savingsAccount;
			}
		}
		return savingsAccount;
	
	}

	@Override
	public void deleteSavingsAccount(long accountNumber) {
		for(SavingsAccount sa: savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				this.savingsAccountList.remove(sa);
			}
		}
	}

	@Override
	public List<SavingsAccount> fetchSavingsAccounts() {

		return this.savingsAccountList;
	}

	@Override
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		for(SavingsAccount sa: savingsAccountList) {
			if (sa.getAccountNumber() == accountNumber) {
				return sa;
			}
		}
		return null;
	}

}
*/
	
	
	
	
	
	
	
	
	
	

