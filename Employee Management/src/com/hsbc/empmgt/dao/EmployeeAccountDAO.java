package com.hsbc.empmgt.dao;

import java.util.Collection;

import com.hsbc.empmgmt.exceptions.EmployeeNotFoundException;
import com.hsbc.empmgmt.exceptions.InsufficientLeavesException;
import com.hsbc.empmgt.model.EmployeeAccount;



public interface EmployeeAccountDAO {

			EmployeeAccount saveEmployeeAccount(EmployeeAccount employeeAccout);
			
		
			EmployeeAccount updateEmployeeAccount(long employeeId, EmployeeAccount employeeAccount);
			
		
		    void deleteEmployeeAccount(long employeeId);
		    
		
		    public Collection<EmployeeAccount> fetchEmpoyeeAccounts();
		
		
		    EmployeeAccount fetchEmployeeAccountByEmployeeId(long employeeId)throws InsufficientLeavesException, EmployeeNotFoundException;

		    
		    EmployeeAccount fetchEmployeeAccountByEmployeeName(String EmployeeName)throws InsufficientLeavesException, EmployeeNotFoundException;
	}

	

