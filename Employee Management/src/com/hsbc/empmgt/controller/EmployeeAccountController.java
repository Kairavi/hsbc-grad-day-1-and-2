package com.hsbc.empmgt.controller;

import java.util.Collection;

import com.hsbc.empmgmt.exceptions.EmployeeNotFoundException;
import com.hsbc.empmgmt.exceptions.InsufficientLeavesException;
import com.hsbc.empmgt.model.EmployeeAccount;
import com.hsbc.empmgt.service.EmployeeAccountService;

public class EmployeeAccountController {

	

		private EmployeeAccountService employeeAccountService ;
		
		
		public EmployeeAccountController( EmployeeAccountService employeeAccountService) {
			this.employeeAccountService = employeeAccountService;
		}

		public EmployeeAccount openEmployeeAccount(String employeeName, double employeeSalary) {
			EmployeeAccount employeeAccount = this.employeeAccountService.createEmployeeAccount(employeeName,  employeeSalary);	
			return employeeAccount;
		}

		public void deleteEmployeeAccount(long employeeId) {
			this.employeeAccountService.deleteEmployeeAccount(employeeId);
		}

		public Collection<EmployeeAccount> fetchEmployeeAccounts() { 
			return this.employeeAccountService.fetchEmpoyeeAccounts();
		}

		public EmployeeAccount fetchEmployeeAccountByEmployeeId(long employeeId) throws InsufficientLeavesException, EmployeeNotFoundException {
			EmployeeAccount employeeAccount = this.employeeAccountService.fetchEmployeeAccountByEmployeeId(employeeId);
			return employeeAccount;
		}

		public double takeLeaves(long EmployeeId, int requestedDaysForLeave) throws InsufficientLeavesException, EmployeeNotFoundException {
			return this.employeeAccountService.takeLeaves(EmployeeId, requestedDaysForLeave);
		}
		

		public double checkLeaves(long EmployeeId,int requestedDaysForLeave) throws EmployeeNotFoundException, InsufficientLeavesException {
			return this.employeeAccountService.checkNoOFLeaves(EmployeeId,requestedDaysForLeave);
		}
		
		
	}
