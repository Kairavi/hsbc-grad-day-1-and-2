public class currentAccountRegister {

    private static currentAccount[] currentAccounts = new currentAccount[] {
            new currentAccount("Naveen", 50_000, "Disha"), new currentAccount("Rajeesh", 10_000),
            new currentAccount("Vinay", 34_000, new Address("8th Ave", "Bangalore", "Karnataka", 577142))

    };

    public static currentAccount fetchSavingsAccountByAccountId(long accountId) {

        for (currentAccount currentAccount : currentAccount) {
            if (currentAccount.getAccountNumber() == accountId) {
                return currentAccount;
            }
        }
        return null;
    }
}