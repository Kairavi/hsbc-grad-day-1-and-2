public class CallByValRef {

    public static void main(String[] args) {

        

        int example1 = 774;
        int example2 = 98;

        
         System.out.printf("Changes before the call   %d %d \n",example1, example2);
         callByValue(example1, example2);
         

         private static void callByValue(int example1, int example2) {
            example1 = example1 +100;
            example2 = example2 +200;
    
            System.out.printf(" changes applied  %d %d \n", operand1, operand2);
    
        }
        System.out.printf("Changes after the call   %d %d \n", example1, example2);



         int [] array = new int[] {29,80,76,767}
         
         

        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
        

        System.out.printf("Changes before the call");
        for (int a : array) {
            System.out.println(a);
        }
    }

    

    private static void callByRef(int[] arr) {
        System.out.println("-------------------------------");
        System.out.print("Changes inside the method");

        arr[0] = 455;
        arr[1] = 5785;
        arr[2] = 696;
        arr[3] = 77;
        for (int a : arr) {
            System.out.println(a);
        }

    }
}