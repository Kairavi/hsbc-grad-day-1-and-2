public class currentAccount {

    //static variables 
    //private static long accountNumberTracker = 1000;

    // instance variables
    private long accountNumber;

    private double accountBalance;

    private String gstNumber;

    private String BusinessName;


    private String customerName;

    private Address address;

    private String emailAddress;

    private String nominee;

    private String nationality;

    public long getAccountNumber() {
        return accountNumber;
    }

    /*
     * Rules of constructor: a. Name of the constructor should be the same as the
     * class name b. The constructot should not have a return type c. If you do not
     * provide a custome constructor, the compiler will create a default no argument
     * constructor c. If you provide an explict constructor, the compiler will not
     * create a default constructor
     * 
     */
    public currentAccount (String customerName, String gstNumber,String BusinessName,long accountNumber) {
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.BusinessName = BusinessName;
        this.accountNumber = accountNumber;
    }

    public currentAccount (String customerName, String gstNumber,String BusinessName,long accountNumber, double accountBalance) {
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.BusinessName = BusinessName;
        this.accountNumber = accountNumber;
        this.accountBalance = accountBalance;
    }

    public currentAccount (String customerName, String gstNumber,String BusinessName,long accountNumber, Address address) {
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.BusinessName = BusinessName;
        this.accountNumber = accountNumber;
        this.address = address;
        
    }

    public currentAccount (String customerName, String gstNumber,String BusinessName,long accountNumber,Address address,double accountBalance) {
        this.customerName = customerName;
        this.accountBalance = accountBalance;
        this.customerName = customerName;
        this.gstNumber = gstNumber;
        this.BusinessName = BusinessName;
        this.accountNumber = accountNumber;
        this.address = address;
    }

    // instance methods
    public double withdraw(double amount) {
        if ((this.accountBalance-50_000) >= amount) {
            this.accountBalance = this.accountBalance - amount;
            return amount;
        }
        return 0;
    }

    public double checkBalance() {
        return this.accountBalance;
    }

    public double deposit(double amount) {
        this.accountBalance = accountBalance + amount;
        return accountBalance;
    }

    public double deposit(double amount, String notes) {
        this.accountBalance = accountBalance + amount;
        System.out.println("notes: " + notes);
        return accountBalance;
    }
     public boolean validate(double accountBalance,double amount){
         return(if(accountBalance>amount));
     }

    public void transferAmount(double amount, long userAccountId) {
         if(validate(this.accountBalance,amount))
         {
        SavingsAccount userAccount = SavingsAccountRegister.fetchSavingsAccountByAccountId(userAccountId);
        // fetch the userAccount based on the accountId
        if(userAccount != NULL)
        {
        // deduct the amount
        this.accountBalance = this.accountBalance - amount;
        // deposit to user's account
        user.accountBalance = user.accountBalance + amount;
        }
      }
    }
    public void updateAddress(Address address) {
        this.address = address;
    }

    public String getCustomerName() {
        return this.customerName;
    }

}