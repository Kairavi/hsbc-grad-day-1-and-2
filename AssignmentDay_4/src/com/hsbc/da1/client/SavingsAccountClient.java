package com.hsbc.da1.client;

import com.hsbc.da1.controller.SavingsAccountController;
import com.hsbc.da1.exception.InsufficientBalanceException;
import com.hsbc.da1.model.SavingsAccount;
import static java.lang.System.*;

public class SavingsAccountClient {
	
	public static void main(String[] args) throws InsufficientBalanceException {
		
		SavingsAccountController controller = new SavingsAccountController();
		
		SavingsAccount kiranSavingsAccount = controller.openSavingsAccount("Kiran", 25_000);
		SavingsAccount rajeshSavingsAccount = controller.openSavingsAccount("Rajesh", 45_000);
		
		try {
		controller.transfer(kiranSavingsAccount.getAccountNumber(), 
				            rajeshSavingsAccount.getAccountNumber(), 45000);
		} catch(InsufficientBalanceException exception) {
			System.out.println(exception.getMessage());
			
		 
		}
		
		try {
			controller.withdraw(kiranSavingsAccount.getAccountNumber(), 45000);
			} catch(CustomerNotFoundException exception) {
				System.out.println(exception.getMessage());
				
			 
			}
			
		
		
		out.println("Account Id "+ kiranSavingsAccount.getAccountNumber());
		out.println("Account Id "+ rajeshSavingsAccount.getAccountNumber());
		
		/*double newAccBalance = controller.deposit(1001, 20000);
		out.println("Account Balance: " + newAccBalance);
		
		SavingsAccount[] savingsAccounts = controller.fetchSavingsAccounts();
		
		for(SavingsAccount savingsAccount: savingsAccounts) {
			if ( savingsAccount != null) {
				out.println("Account : "+ savingsAccount);
				
			}
		}*/
	}
}
