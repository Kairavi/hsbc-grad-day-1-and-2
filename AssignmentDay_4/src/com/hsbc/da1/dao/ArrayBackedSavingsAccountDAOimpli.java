package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;


public class ArrayBackedSavingsAccountDAOImpl implements SavingsAccountDAO {

	private static SavingsAccount[] savingsAccounts = new SavingsAccount[100];
	private static int counter;
	
	public SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount) {
		savingsAccounts[counter++] = savingsAccount;
		return savingsAccount;
	}
	
	public SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount) {
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = savingsAccount;
				break;
			}
		}
		return savingsAccount;
	}
	
	public void deleteSavingsAccount(long accountNumber) {
		
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccounts[index].getAccountNumber() == accountNumber) {
				savingsAccounts[index] = null;
				break;
			}
		}
	}
	
	public SavingsAccount[] fetchSavingsAccounts() throws CustomerNotFoundException{
		
		return savingsAccounts;
		else {
			throw new InsufficientBalanceException("Customer doesn't exist");
		}
	}
	
	public SavingsAccount fetchSavingsAccountByAccountId(long accountNumber) {
		for (int index = 0; index < savingsAccounts.length; index ++) {
			if ( savingsAccount[index]!=null && savingsAccounts[index].getAccountNumber() == accountNumber) {
				return savingsAccounts[index];
			}
		}
		throw new CustomerNotFoundException("Invalid customer id");
	}

}
