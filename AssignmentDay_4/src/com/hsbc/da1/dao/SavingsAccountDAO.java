package com.hsbc.da1.dao;

import com.hsbc.da1.model.SavingsAccount;
import java.util.List;
import java.util.*; 

public interface SavingsAccountDAO {
	
	
	SavingsAccount saveSavingsAccount(SavingsAccount savingsAccount);
	
	SavingsAccount updateSavingsAccount(long accountNumber, SavingsAccount savingsAccount);
	
	void deleteSavingsAccount(long accountNumber);
	
	 public List<SavingsAccount> fetchSavingsAccounts();
	
	
	SavingsAccount fetchSavingsAccountByAccountId(long accountNumber)throws CustomerNotFoundException;

}

		
		
		
		
		
		
		
		
		
	
